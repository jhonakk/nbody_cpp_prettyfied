#ifndef STOPWATCH_HPP
#define STOPWATCH_HPP

#include <chrono>

using clock_n = std::chrono::high_resolution_clock;

class Stopwatch {
	public:
		Stopwatch();
		~Stopwatch();
		
		auto elapsed_and_reset() -> double;
	
	private:
		clock_n::time_point last_;
};

#endif
