#include <iostream>

#include "vector.hpp"

namespace struct_of_arrays {

    
    inline void p_p_interaction(...);
    void update(...);
    void move(...);
    void run();
}