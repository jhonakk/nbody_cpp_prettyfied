#ifndef VECTOR_HPP
#define VECTOR_HPP

class Vec {
	public:
		Vec(float x, float y, float z);
		~Vec();
		
		auto operator*=(float s) -> Vec&;
		auto operator*=(const Vec& v) -> Vec&;
		auto operator+=(const Vec& v) -> Vec&;
		auto operator-=(const Vec& v) -> Vec&;
		
		auto operator+(const Vec& b) const -> Vec;
		auto operator-(const Vec& b) const -> Vec;
		auto operator*(float s) const -> Vec;
		auto operator*(const Vec& b) const -> Vec;

        inline auto get_x() const -> float {return x_;}
        inline auto get_y() const -> float {return y_;}
        inline auto get_z() const -> float {return z_;}

        void set_x(float x) {x_ = x;}
        void set_y(float y) {y_ = y;}
        void set_z(float z) {z_ = z;}

	private:
		float x_;
		float y_;
		float z_;
};

#endif
