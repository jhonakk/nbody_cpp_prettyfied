#include <iostream>

#include "shared.hpp"
#include "array_of_structs.hpp"
#include "struct_of_arrays.hpp"
#include "array_of_structs_of_arrays.hpp"
#include "vector.hpp"
#include "stopwatch.hpp"

int main(void) {
	std::cout << kProblemSize / 1000 << "k particles "
              << "(" << kProblemSize * sizeof(float) * 7 / 1024 << "kiB)\n";

    array_of_structs::run();
    //struct_of_arrays::run();
    //array_of_structs_of_arrays::run();

    return 0;
}
