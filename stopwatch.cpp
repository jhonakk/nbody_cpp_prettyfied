#include "stopwatch.hpp"

Stopwatch::Stopwatch() {
	last_ = clock_n::now();
}

Stopwatch::~Stopwatch() = default;

auto Stopwatch::elapsed_and_reset() -> double {
	const auto now = clock_n::now();
    const auto seconds = std::chrono::duration<double>(now - last_).count();
    last_ = now;
    return seconds;
}
