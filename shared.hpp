#include <random>

#include "stopwatch.hpp"

#if defined __GNUC__
#    define IVDEP _Pragma("GCC ivdep")
#elif defined(_MSC_VER)
#    define IVDEP __pragma(loop(ivdep))
#elif defined __clang__
#    define IVDEP _Pragma("clang loop vectorize(enable) interleave(enable) distribute(enable)")
#else
#    error "Please define IVDEP for your compiler!"
#endif

constexpr auto kProblemSize = 16 * 1024;
constexpr auto kTimestep = 0.0001f;
constexpr auto kSteps = 5;
constexpr auto kEPS2 = 0.01f;

constexpr auto kLanes = 16;
constexpr auto kBlocks = kProblemSize / kLanes;
