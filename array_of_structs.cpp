#include "shared.hpp"
#include "vector.hpp"

#include "array_of_structs.hpp"

void array_of_structs::p_p_interaction(Particle& p_i, const Particle& p_j) {
	Vec distance = p_i.pos - p_j.pos;
    Vec distance_sqr = distance * distance;
    const float dist_sqr = kEPS2 + distance_sqr.get_x() + distance_sqr.get_y() + distance_sqr.get_z();
    const float dist_sixth = dist_sqr * dist_sqr * dist_sqr;
    const float inv_dist_cube = 1.0f / std::sqrt(dist_sixth);
    const float sts = p_j.mass * inv_dist_cube * kTimestep;
    p_i.vel += distance_sqr * sts;
}

void array_of_structs::update(std::vector<Particle>& particles) {
	IVDEP
    for (std::size_t i = 0; i < particles.size(); i++)
        for (std::size_t j = 0; j < particles.size(); j++)
            p_p_interaction(particles[i], particles[j]);
}

void array_of_structs::move(std::vector<Particle>& particles) {
	IVDEP
    for (std::size_t i = 0; i < particles.size(); i++)
        particles[i].pos += particles[i].vel * kTimestep;
}

void array_of_structs::run() {
	std::vector<Particle> particles;
	particles.reserve(kProblemSize);

    std::default_random_engine engine;
    std::normal_distribution<float> dist(0.0f, 1.0f);
    for (std::size_t i = 0; i < kProblemSize; i++) {
        particles.push_back(Particle{Vec(dist(engine), dist(engine), dist(engine)),
									 Vec(dist(engine) / 10.0f, dist(engine) / 10.0f, dist(engine) / 10.0f),
									 dist(engine) / 100.0f});
	}
    Stopwatch watch;
    double sum_update = 0;
    double sum_move = 0;
    for (std::size_t s = 0; s < kSteps; ++s)
    {
        update(particles);
        sum_update += watch.elapsed_and_reset();
        move(particles);
        sum_move += watch.elapsed_and_reset();
    }
    std::cout << "AoS\t" << sum_update / kSteps << '\t' << sum_move / kSteps << '\n';
}
