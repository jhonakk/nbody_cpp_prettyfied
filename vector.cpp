#include "vector.hpp"

Vec::Vec(float x, float y, float z) {
	x_ = x;
	y_ = y;
	z_ = z;
}

Vec::~Vec() = default;



auto Vec::operator*=(float s) -> Vec& {
	x_ *= s;
    y_ *= s;
    z_ *= s;
    return *this;
}

auto Vec::operator*=(const Vec& v) -> Vec& {
	x_ *= v.x_;
    y_ *= v.y_;
    z_ *= v.z_;
    return *this;
}

auto Vec::operator+=(const Vec& v) -> Vec& {
	x_ += v.x_;
    y_ += v.y_;
    z_ += v.z_;
    return *this;
}

auto Vec::operator-=(const Vec& v) -> Vec& {
	x_ -= v.x_;
    y_ -= v.y_;
    z_ -= v.z_;
    return *this;
}



auto Vec::operator+(const Vec& b) const -> Vec {
	auto copy = *this;
    return copy += b;
}

auto Vec::operator-(const Vec& b) const -> Vec {
    auto copy = *this;
    return copy -= b;
}

auto Vec::operator*(float s) const -> Vec {
	auto copy = *this;
    return copy *= s;
}

auto Vec::operator*(const Vec& b) const -> Vec {
    auto copy = *this;
    return copy *= b;
}

