#include <iostream>
#include <vector>

#include "vector.hpp"

namespace array_of_structs {
	struct Particle {
        Vec pos;
        Vec vel;
        float mass;
    };
    
	inline void p_p_interaction(Particle& p_i, const Particle& p_j);
	void update(std::vector<Particle>& particles);
	void move(std::vector<Particle>& particles);
	void run();
}
